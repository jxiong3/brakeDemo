#
# Brake geometry ScriptWrapper
#
# @version: Training
# @description: Brake pad geometry. Icon made by Freepik from www.flaticon.com.
# @author: Phoenix Integration
# @icon: cad.ico

# define variables
variable: centerLength double input default=4 lowerbound=0 units="inches" description="arc length of brake pad"
variable: thickness double input default=0.3 lowerbound=0 units="inches" description="brake pad thickness"
variable: width double input default=2 lowerbound=0 units="inches" description="brake pad width"
variable: od double input default=11 lowerbound=4 units="inches" description="diameter of rotor"
variable: resolution int input default=50 lowerbound=0 description="The number of segments in an arc. The higher the number the better the image."
variable: showFins boolean input default=false description="hide the rear portion of the rotor, exposing the fins"
variable: brake file output description="brake geometry" extension="stl"
variable: rotor file output description="rotor geometry" extension="stl"
variable: fins file output description="fin geometry" extension="stl"

# STL files don't support color.
#setgroup "Thermo"
#variable: heat double input default=40 lowerbound=0 units="kW" description="heat output of the brake"
#variable: maxHeat double input default=60 lowerbound=0 units="kW" description="maximum heat allowed"
#variable: maxHeat double input default=60 lowerbound=0 units="kW" description="maximum heat allowed"

setGroup "FinDesign"
variable: finCount int input default=27 lowerbound=0 description="number of fins"
variable: finLength double input default=2 lowerbound=0 units="inches" description="fin length"
variable: finWidth double input default=0.2 lowerbound=0 units="inches" description="fin width"
variable: finAngle double input default=30 lowerbound=-90 upperbound=90 units="degrees" description="angle of the fins"
variable: finError double output units="inches" description="fin overhang"

script: language=java

/**
 * Platform indipendent new line separator constant.
 */
final String NEW_LINE = System.getProperty("line.separator");

/**
 * The thickness of the rotor.
 */
final double ROTOR_THICKNESS = 0.2;

/**
 * The thickness of the fins.
 */
final double FIN_THICKNESS = 0.2;

/**
 * The height of the upper part of the rotor.
 */
final double ROTOR_UPPER_HEIGHT = 1.5;

/**
 * The radius of the smaller upper part of the rotor. Should be equal to the minimum radius.
 */
final double ROTOR_UPPER_RADIUS = 2.0;

/**
 * Generate the geometry.
 */
void run()
{
   _makeBrake();
   _makeRotor();
   _makeFins();
}

/**
 * Component clean up.
 */
void onEnd()
{
   // Nothing to clean up
}

/**
 * Generates the genometry file for the brake.
 */
void _makeBrake()
{
   double z1;
   double z2;

   StringBuilder sb = new StringBuilder();

   // Write the start of the geometry
    _appendLine(sb, "solid brake");

   int res = Math.toIntExact(resolution.value);

   // Draw pad 1
   z1 = 0;
   z2 = -thickness.value;
   _drawPad(sb, od.value, width.value, centerLength.value, z1, z2, res);

   // Draw Pad 2
   z1 = 2 * ROTOR_THICKNESS + FIN_THICKNESS;
   z2 = z1 + thickness.value;
   _drawPad(sb, od.value, width.value, centerLength.value, z1, z2, res);

   // Write the end of the geometry
   _appendLine(sb, "endsolid");

   brake.fromString(sb.toString());
}

/**
 * Draws a brake pad.
 * @param sb The string builder to which the geometry should be written.
 * @param od The outer diameter of the pad.
 * @param width The width of the pad.
 * @param centerLength The center length of the pad.
 * @param z1 The lower height of the pad.
 * @param z2 The upper height of the pad.
 * @param resolution The number of segments in a full circle.
 */
void _drawPad(StringBuilder sb, double od, double width, double centerLength, double z1, double z2, int resolution)
{
   // outer radius
   double or = od / 2.0;

   // inner radius
   double ir = or - width;

   // Determine effective radius
   double effectiveRadius = or - width/ 2.0;

   // Determine total angle using center length and effective radius.
   double totalAngle = centerLength / effectiveRadius;

   // Determine how many segments are required for resolution.
   int numSegments = (int)Math.round(resolution * totalAngle / (2.0 * Math.PI));
   
   // Draw bottom
   _drawPadSurface(sb, or, ir, totalAngle, z1, numSegments);

   // Draw top
   _drawPadSurface(sb, or, ir, totalAngle, z2, numSegments);

   // draw inner edge
   _drawArcEdge(sb, ir, totalAngle, z1, z2, numSegments);

   // draw outer edge
   _drawArcEdge(sb, or, totalAngle, z1, z2, numSegments);

   // draw end 1
   double startAngle = 0;
   _drawArcEnd(sb, ir, or, z1, z2, startAngle);

   // draw end 2
   _drawArcEnd(sb, ir, or, z1, z2, totalAngle);
}

/**
 * Draws the top or bottom of the pad.
 * @param sb The string builder to which the geometry should be written.
 * @param or The outer radius of the pad.
 * @param ir The innter radius of the pad.
 * @param totalAngle The total angle of the pad arc.
 * @param z TThe z coordinate of the pad surface
 * @param numSegments The number of segments in the arc.
 */
void _drawPadSurface(StringBuilder sb, double or, double ir, double totalAngle, double z, int numSegments)
{
   double anglePart = totalAngle / (double)numSegments;

   // Loop through the angles to draw the triangles of the edge.
   for (int j = 0; j < numSegments; ++j)
   {
      double endAngle = anglePart * (double)(j + 1);
      double x1 = ir * Math.sin(endAngle);
      double y1 = ir * Math.cos(endAngle);

      double x2 = or * Math.sin(endAngle);
      double y2 = or * Math.cos(endAngle);

      double startAngle = anglePart * (double)j;
      double x3 = or * Math.sin(startAngle);
      double y3 = or * Math.cos(startAngle);

      double x4 = ir * Math.sin(startAngle);
      double y4 = ir * Math.cos(startAngle);

      double[] p1 =  { x1, y1, z };
      double[] p2 =  { x2, y2, z };
      double[] p3 =  { x3, y3, z };
      double[] p4 =  { x4, y4, z };

      _createLoop(sb, p1, p2, p3);
      _createLoop(sb, p3, p4, p1);
   }
}

/**
 * Draws the inner or outer edge of the pad.
 * @param sb The string builder to which the geometry should be written.
 * @param r The radius of the edge.
 * @param angle The total angle of the pad arc.
 * @param z1 TThe z coordinate of the top pad surface
 * @param z2 TThe z coordinate of the bottom pad surface
 * @param sliceCount The number of segments in the arc.
 */
void _drawArcEdge(StringBuilder sb, double r, double angle, double z1, double z2, int sliceCount)
{
   double anglePart = angle / (double)sliceCount;

   // Loop through the angles to draw the triangles of the edge.
   for (int j = 0; j < sliceCount; ++j)
   {
      double endAngle = anglePart * (double)(j + 1);
      double x1 = r * Math.sin(endAngle);
      double y1 = r * Math.cos(endAngle);

      double startAngle = anglePart * (double)j;
      double x2 = r * Math.sin(startAngle);
      double y2 = r * Math.cos(startAngle);

      double[] p1 =  { x1, y1, z1 };
      double[] p2 =  { x2, y2, z1 };
      double[] p3 =  { x2, y2, z2 };
      double[] p4 =  { x1, y1, z2 };

      _createLoop(sb, p1, p2, p3);
      _createLoop(sb, p3, p4, p1);
   }
}

/**
 * Draws one of the end of the pad.
 * @param sb The string builder to which the geometry should be written.
 * @param or The outer radius of the pad.
 * @param ir The innter radius of the pad.
 * @param z1 TThe z coordinate of the top pad surface
 * @param z2 TThe z coordinate of the bottom pad surface
 * @param angle The angle at which the ege should be drawn.
 */
void _drawArcEnd(StringBuilder sb, double ir, double or, double z1, double z2, double angle)
{
   double x1 = or * Math.sin(angle);
   double y1 = or * Math.cos(angle);
   double x2 = ir * Math.sin(angle);
   double y2 = ir * Math.cos(angle);

   double[] p1 =  { x1, y1, z1 };
   double[] p2 =  { x2, y2, z1 };
   double[] p3 =  { x2, y2, z2 };
   double[] p4 =  { x1, y1, z2 };

   _createLoop(sb, p3, p2, p1);
   _createLoop(sb, p1, p4, p3);
}

/**
 * Generates the genometry file for the rotor.
 */
void _makeRotor()
{
   double z1;
   double z2;
   double radius = od.value / 2.0;

   StringBuilder sb = new StringBuilder();

   // Write the start of the geometry
    _appendLine(sb, "solid rotor");

   z1 = 0;
   z2 = ROTOR_THICKNESS;

   if (!showFins.value)
   {
      // Draw the bottom of the rotor
      _drawCylinder(sb, radius, z1, z2);
   }

   // Draw the top of the brake (lower wide disk)
   z1 = z2 + FIN_THICKNESS;
   z2 = z1 + ROTOR_THICKNESS;

   _drawCylinder(sb, radius, z1, z2);

   // Draw the top of the rotor (upper smaller diameter disk)
   z1 = z2;
   z2 = z2 + ROTOR_UPPER_HEIGHT;

   _drawCylinder(sb, ROTOR_UPPER_RADIUS, z1, z2);

   // Write the end of the geometry
   _appendLine(sb, "endsolid");

   rotor.fromString(sb.toString());
}

/**
 * Writes the geometry of a cylinder to string builder.
 * @param sb The string builder
 * @param r The radius of the circle
 * @param z1 the z coordinate of the bottom of the circle.
 * @param z2 the z coordinate of the top of the circle.
 */
void _drawCylinder(StringBuilder sb, double r, double z1, double z2)
{
   // Draw bottom of cylinder
   _drawCircle(sb, r, z1);

   // Draw top of cylinder
   _drawCircle(sb, r, z2);

   // Draw edge of cylinder
   _drawEdge(sb, r, z1, z2);
}

/**
 * Draws a circle to the string builder.
 * @param sb The string builder
 * @param r The radius of the circle
 * @param z the z coordinate of the circle.
 */
void _drawCircle(StringBuilder sb, double r, double z)
{
   int sliceCount = resolution.value;
   double angle = 2.0 * Math.PI / ((double)sliceCount);

   // Loop through the angles to draw the triangles of the disk.
   for (int j = 0; j < sliceCount; ++j)
   {
      double endAngle = angle * (double)(j + 1);
      double x1 = r * Math.sin(endAngle);
      double y1 = r * Math.cos(endAngle);

      double startAngle = angle * (double)j;
      double x2 = r * Math.sin(startAngle);
      double y2 = r * Math.cos(startAngle);

      double[] center =  { 0.0, 0.0, z };
      double[] p1 =  { x1, y1, z };
      double[] p2 =  { x2, y2, z };

      _createLoop(sb, center, p1, p2);
   }
}

/**
 * Draws the edge of a cylinder.
 * @param sb The string builder
 * @param r The radius of the circle
 * @param z1 the z coordinate of the bottom of the circle.
 * @param z2 the z coordinate of the top of the circle.
 */
void _drawEdge(StringBuilder sb, double r, double z1, double z2)
{
   int sliceCount = resolution.value;
   double angle = 2.0 * Math.PI / ((double)sliceCount);

   // Loop through the angles to draw the triangles of the edge.
   for (int j = 0; j < sliceCount; ++j)
   {
      double endAngle = angle * (double)(j + 1);
      double x1 = r * Math.sin(endAngle);
      double y1 = r * Math.cos(endAngle);

      double startAngle = angle * (double)j;
      double x2 = r * Math.sin(startAngle);
      double y2 = r * Math.cos(startAngle);

      double[] p1 =  { x1, y1, z1 };
      double[] p2 =  { x2, y2, z1 };
      double[] p3 =  { x2, y2, z2 };
      double[] p4 =  { x1, y1, z2 };

      // Use two triangles to make a square.
      _createLoop(sb, p1, p2, p3);
      _createLoop(sb, p3, p4, p1);
   }
}

/**
 * Generates the genometry file for the fins.
 */
void _makeFins()
{
   double radius = od.value / 2.0;
   double length = FinDesign.finLength.value;
   double width = FinDesign.finWidth.value;
   double finAngle = Math.toRadians(FinDesign.finAngle.value);
   int numFins = Math.toIntExact(FinDesign.finCount.value);

   // Fin midpoint radius
   double finMid = ((ROTOR_UPPER_RADIUS + (radius - ROTOR_UPPER_RADIUS) / 2.0));

   // The contribution to the fin tip radius from the length of the fin.
   double finRight = finMid + (length / 2.0) * Math.cos(finAngle);

   // The contribution to the fin tip radius from the width of the fin.
   double finTop = (width / 2.0) * Math.sin(finAngle);

   // The fin tip radius
   double finTipRadius = Math.sqrt(finRight * finRight + finTop * finTop);

   if(radius < finTipRadius)
   {
      // Set the error value to the lenght of the fin beyond the rotor.
      FinDesign.finError.value = finTipRadius - radius;
   }
   else
   {
      // Set the error to zer if there is no excess.
      finError.value = 0;
   }

   StringBuilder sb = new StringBuilder();

   // Write the start of the geometry
    _appendLine(sb, "solid fins");

   for (int i = 0; i < numFins; ++i)
   {
      double alpha = 2.0 * Math.PI * (double)i / (double)numFins;

      // Draw fin
      _drawFin(sb, radius, length, width, finAngle, alpha);
   }

   // Write the end of the geometry
   _appendLine(sb, "endsolid");

   fins.fromString(sb.toString());
}

/**
 * Draws a single fin.
 * @param sb The string builder to which the fin should be added.
 * @param rotorRadius The radius of the rotor.
 * @param finLength The length of the fin.
 * @param finWidth The width of the fin.
 * @param finAngle The angle of the fin.
 * @param finLocation The angluar position of the fin in radians.
 */
void _drawFin(
   StringBuilder sb, double rotorRadius, double finLength, double finWidth, double finAngle, double finLocation)
{
   // The locationof the fin centerpoint
   double finMidpointRadius = ((ROTOR_UPPER_RADIUS + (rotorRadius - ROTOR_UPPER_RADIUS) / 2.0));
   double x = finMidpointRadius * Math.cos(finLocation);
   double y = finMidpointRadius * Math.sin(finLocation);

   double finAbsoluteAngle = finLocation + finAngle;
   double halfLength = finLength / 2.0;
   double halfWidth = finWidth / 2.0;

   double x1 = x - halfLength * Math.cos(finAbsoluteAngle);
   double y1 = y - halfLength * Math.sin(finAbsoluteAngle);
   double x2 = x + halfLength * Math.cos(finAbsoluteAngle);
   double y2 = y + halfLength * Math.sin(finAbsoluteAngle);
   double x3 = x2 - halfWidth * Math.sin(finAbsoluteAngle);
   double y3 = y2 + halfWidth * Math.cos(finAbsoluteAngle);
   double x4 = x1 - halfWidth * Math.sin(finAbsoluteAngle);
   double y4 = y1 + halfWidth * Math.cos(finAbsoluteAngle);
   double z1 = ROTOR_THICKNESS;
   double z2 = z1 + FIN_THICKNESS;

   // Eight points define a box
   double[] p1 =  { x1, y1, z1 };
   double[] p2 =  { x1, y1, z2 };
   double[] p3 =  { x2, y2, z2 };
   double[] p4 =  { x2, y2, z1 };
   double[] p5 =  { x3, y3, z2 };
   double[] p6 =  { x3, y3, z1 };
   double[] p7 =  { x4, y4, z2 };
   double[] p8 =  { x4, y4, z1 };

   // Side 1
   _createLoop(sb, p1, p2, p3);
   _createLoop(sb, p3, p4, p1);

   // Side 2
   _createLoop(sb, p4, p3, p5);
   _createLoop(sb, p5, p6, p4);

   // Side 3
   _createLoop(sb, p6, p5, p7);
   _createLoop(sb, p7, p8, p6);

   // Side 4
   _createLoop(sb, p8, p7, p2);
   _createLoop(sb, p2, p1, p8);

   // Side 5
   _createLoop(sb, p2, p3, p5);
   _createLoop(sb, p5, p7, p2);

   // Side 6
   _createLoop(sb, p1, p4, p6);
   _createLoop(sb, p6, p8, p1);
}

/**
 * Helper method to write a line to a string builder.
 */
void _appendLine(StringBuilder sb, String line)
{
   sb.append(line);
   sb.append(NEW_LINE);
}

/**
 * Appends a loop to the string builder.
 *
 * Each point in the loop is assumed to have exactly three values.
 *
 * @param sb The string builder to which the loop should be added.
 * @param p1 The first point in the loop.
 * @param p2 The second point in the loop.
 * @param p3 The third point in the loop.
 */
void _createLoop(StringBuilder sb, double[] p1, double[] p2, double[] p3)
{
   // Create vecotrs from the points.
   double[] a = { p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2] };
   double[] b = { p3[0] - p1[0], p3[1] - p1[1], p3[2] - p1[2]};

   double[] n = _normal(a, b);

   _appendLine(sb, String.format(Locale.US, "   facet normal %f %f %f", new Object[] { n[0], n[1], n[2] }));
   _appendLine(sb, "      outer loop");
   _appendLine(sb, String.format(Locale.US, "         vertex %f %f %f", new Object[] { p1[0], p1[1], p1[2] }));
   _appendLine(sb, String.format(Locale.US, "         vertex %f %f %f", new Object[] { p2[0], p2[1], p2[2] }));
   _appendLine(sb, String.format(Locale.US, "         vertex %f %f %f", new Object[] { p3[0], p3[1], p3[2] }));
   _appendLine(sb, "      endloop");
   _appendLine(sb, "   endfacet");
}

/**
 * Calculates a unit vector normal to the given vectors.
 * @param a The first vector.
 * @param b The second vector.
 */
double[] _normal(double[] a, double[] b)
{
   // Perform the cross product.
   double newA = (a[1] * b[2]) - (a[2] * b[1]);
   double newB = (a[0] * b[2]) - (a[2] * b[0]);
   double newC = (a[0] * b[1]) - (a[1] * b[0]);

   // Get the magintude to create the unit vector.
   double mag = Math.sqrt(newA * newA + newB * newB + newC * newC);

   return new double[] { Math.abs(newA / mag), Math.abs(newB / mag), Math.abs(newC / mag) };
}